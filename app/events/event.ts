export class Event{
    date : number;
    description: string;
    name : string;

    constructor(name, description : string, date : number){
        this.name = name;
        this.description = description;
        this.date = date;
    }
}