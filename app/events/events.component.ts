import { Component, OnInit } from "@angular/core";
import { Event } from './event';
import { Observable } from 'rxjs/Observable';
import firebase = require('nativescript-plugin-firebase');
import { EventsService } from '../services/events.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
    selector: "ns-events",
    moduleId: module.id,
    templateUrl: "./events.component.html",
})
export class EventsComponent implements OnInit {

    events$ : Observable<any>;

    constructor(private service : EventsService, private router : Router){}

    ngOnInit(){
        this.events$ = this.service.getEvents();

        this.events$.subscribe((event : any)=>{
            console.log("Evento desde el observador");
            console.log(event);
        })
        
    }

    openEvent(event : Event){

        let params : NavigationExtras = {
            queryParams: {
                event: JSON.stringify(event)
            }
        }

        this.router.navigate(["event"],params);
    }

    
  
    

}
