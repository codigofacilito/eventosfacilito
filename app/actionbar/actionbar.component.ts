import { Component, Output, EventEmitter } from "@angular/core";
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
@Component({
    selector: "ns-actionbar",
    moduleId: module.id,
    templateUrl: "./actionbar.component.html",
})
export class ActionbarComponent {

    @Output()
    menuTriggered  : EventEmitter<string> = new EventEmitter();

    constructor( private auth : AuthService, private router : Router ){}

    openEventForm(){
        this.router.navigate(["new"]);
    }

    openDrawer(){
        this.menuTriggered.emit("");
    }

}
