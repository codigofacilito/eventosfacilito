import { Injectable } from '@angular/core';
import { getString, setString, remove } from 'application-settings';
import firebase = require('nativescript-plugin-firebase');

@Injectable()
export class AuthService{
    constructor(){}

    isLoggedIn() : boolean{
        return AuthService.token != null;
    }

    login(token : string){
        AuthService.token = token;
    }

    logout() : Promise<any>{
        AuthService.token = null;
        return firebase.logout();

    }

    static get token() : string | null{
        return getString("token");
    }

    static set token(token : string){
        if(token == null) { 
            remove("token");
        }else{
            setString("token",token);
        }
        
    }

}