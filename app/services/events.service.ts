import { Injectable, NgZone } from '@angular/core';
import firebase = require('nativescript-plugin-firebase');
import { Observable } from 'rxjs/Observable';
import { Event } from '../events/event';

@Injectable()
export class EventsService{

    
    private _events : Array<Event> = [];
    private _eventsControl = {};

    constructor(private ngZone : NgZone){}

    getEvents(){
        return new Observable((observer : any)=>{

            let onNewValue = (data : any)=>{
                
                this.ngZone.run(()=>{
                    let results = this.handleResponse(data);
                    observer.next(results);
                })
                
            }

            
            firebase.addValueEventListener(onNewValue,'/events');

        });
    }

    handleResponse(data : any){
        if(data && data.value){
            for(let id in data.value){
                if(this._eventsControl[id]) continue;

                let eventJSON = data.value[id];
                let event = new Event(eventJSON.name,eventJSON.description,eventJSON.date);
                
                this._events.push(event);
                this._eventsControl[id] = true;
                
            }
        }

        return this._events;
    }


}