import { Component } from "@angular/core";
import { AuthService } from '../services/auth.service';
import { DatePicker } from 'ui/date-picker';
import { TimePicker } from 'ui/time-picker';

import firebase = require('nativescript-plugin-firebase');

@Component({
    selector: "ns-events-form",
    moduleId: module.id,
    templateUrl: "./eventsform.component.html",
})
export class EventsFormComponent {

    name : string;
    description : string;
    datepicker : DatePicker;
    timepicker : TimePicker;

    constructor(){}

    setDatePicker(datePickerComponet : DatePicker){
        console.log("Setting DatePicker");
        console.log(datePickerComponet);
        this.datepicker = datePickerComponet;
    }

    setTimePicker(timePickerComponet : TimePicker){
        this.timepicker = timePickerComponet;
    }

    add(){
        console.log("Datos: ")
        let date = new Date(this.datepicker.year, this.datepicker.month,this.datepicker.day,
                                this.timepicker.hour,this.timepicker.minute,0);
        

        firebase.push('/events',{
            name: this.name,
            description: this.description,
            date: date.getTime()
        });
    }
    
    

}
