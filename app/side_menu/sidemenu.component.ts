import { Component } from "@angular/core";
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
@Component({
    selector: "ns-side-menu",
    moduleId: module.id,
    templateUrl: "./sidemenu.component.html",
})
export class SideMenuComponent {
    constructor( private auth : AuthService, private router : Router ){}
    
    
    logout(){
        this.auth.logout().then(()=>{
            this.router.navigate(['login']);
        })
    }

}
