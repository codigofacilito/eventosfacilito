import { Component, OnInit } from "@angular/core";
import { Event } from '../events/event';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: "ns-event",
    moduleId: module.id,
    templateUrl: "./event.component.html",
})
export class EventComponent implements OnInit {

    event : Event;    

    constructor(private route : ActivatedRoute){}

    ngOnInit(){
        this.route.queryParams.subscribe(params => {
            console.log("Parametros de query");
            console.log(params["event"]);

            this.event = JSON.parse(params["event"]) as Event ;
        });
    }

    
  
    

}
