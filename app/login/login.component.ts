import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular/router';
import firebase = require('nativescript-plugin-firebase');

@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
})
export class LoginComponent implements OnInit {
    email : string;
    password : string;
    
    constructor(private router : RouterExtensions ) { }

    goToSignUp(){
        this.router.navigate(['signup']);
    }

    login(){
        firebase.login({
            type: firebase.LoginType.PASSWORD,
            email: this.email,
            password: this.password
        }).then(result => {
            this.router.navigate([''],{clearHistory: true});
        }).catch(err => console.log(err));
    }

    ngOnInit(): void {
    }
}
